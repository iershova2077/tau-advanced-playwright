# Use the official Microsoft Playwright image which includes Node.js and browsers
FROM mcr.microsoft.com/playwright:v1.17.1-focal

# Set the working directory inside the Docker container
WORKDIR /tests

# Copy package.json and package-lock.json (or yarn.lock) into the container
COPY package*.json ./

# Install dependencies
# If you're using yarn, use the command 'RUN yarn install'
RUN npm install

# If you need to install browsers for Playwright, uncomment the line below
# RUN npx playwright install

# Bundle the source code inside the Docker image, 
#Copy the rest of your test suite files into the container
COPY . .

# Build the TypeScript project
# This step assumes you have a script in your package.json to build the TypeScript project
RUN npm run build

# Set the command to run your test
# Replace `your-test-script` with your actual test running script
CMD ["npm", "run", "test-ui"]

# Define the command to run your tests
# This assumes you have a test script in your package.json
#CMD ["npm", "test"]